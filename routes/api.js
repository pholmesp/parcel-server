var express = require('express');
var router = express.Router();
var path = require('path');
var fs = require('fs');
var multer  = require('multer');
var dbServices = require('../moudle/db-service');
var passport = require('../config/passport');
var mail = require('../moudle/qqmail');
var dateFormat = require('dateformat');
var zipFiles = require('../moudle/zipFiles');

/* GET home page. */
router.get('/doc', function(req, res, next) {
  res.sendFile(path.join(__dirname, '../client/build/index.html'));
  // fs.createReadStream(path.join(__dirname, '../client/build/index.html')).pipe(res);
});

router.get('/log', function(req,res,next){
  console.log(__dirname)
  fs.createReadStream(path.join(__dirname, '../nohup.out'))
  .pipe(res);
});

router.post('/updateOrder', function(req,res){
  let key = req.body.key
  let value=req.body.value
  let idorder = req.body.idorder
  let fields = req.body.fields
  let section = []
  for(let field of fields){
    section.push(` ${field.key}='${field.value}' `)
  }
  section = section.join(',')
  let sql = `UPDATE orders SET ${section} WHERE idorder=${idorder}`
  console.log(sql)
  dbServices.query(sql, function(error, data){
    if(error) res.json(error)
    else{
      res.json(data)
    }
  })
})

router.post('/login',
  passport.authenticate('user'),
  function(req, res) {
    console.log('login', req.user)
    res.json(req.user);
  })
router.get('/currentUser', passport.authorize('user'),
  function(req, res) {
    console.log('currentUser', req.user)
    res.json(req.user);
  })
router.post('/editUser',
  function(req, res) {
    console.log(req.user, req.user_f)
    res.json(req.user);
  })
router.get('/logout', (req, res)=> {
  req.logOut();
  res.json({'logout':'success'})
  // res.redirect('/');
  console.log('logout');
  //res.json({success: true, msg: 'Successful signed out.'})
  
});

router.post('/neworder', passport.authenticate('jwtUser'),function(req, res) {
  var a =0;
  //var sql = 'insert into orders set ?'
  var day = dateFormat(new Date(), "isoDateTime");
  var sql = 'INSERT INTO orders (creatTime, totalPrice, tableID, status, type, comment) VALUES (?, ?, ?, ?, ?, ?)';
  //console.log(req.body.id)
  console.log(req.body.creatTime)
  dbServices.query(sql, [day, req.body.totalPrice, req.body.tableID, 1, 1, req.body.comment], function(error,dd){
        
  if(error){
    return res.json(error);
  }
  else{
    var id  = dd.insertId;
    
    //res.json({success: true, msg: id});
    
    var v = req.body.items;
    var objs = [];
    for (let i =0;i<v.length;i++){
      var obj = [v[i].num, id, v[i].DishID, v[i].type, v[i].comment];
      // var obj ={};
      // obj.orderID = id;
      // obj.DishID = v[i].DishID;
      // obj.DishCount = v[i].num;
      objs.push(obj);
    }
    console.log("297 " + objs);
    var sq = 'INSERT INTO Items (DishCount, orderID, DishID, type, comment) VALUES ?'
    
    dbServices.query(sq, [objs], function(error){    
      if(error){      
        return res.json(error); 
      } else {
        var sqll = 'update tables set ? where id = ?';
        dbServices.query(sqll, [{status: 2, currentOrderID: id},req.body.tableID], function(error){
                
          if(error){
            return res.json(error);
          }
          else {
            res.json({success: true, msg: 'Successfully created the new order'});
            
          }
        });
      }
    })
  }   
})
});
    
var createFolder = function(folder){
  try{
    fs.accessSync(folder);
  }catch(e){
    fs.mkdirSync(folder);
  }
};

var uploadFolder = 'data/img/';
createFolder(uploadFolder);
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, uploadFolder);    // 保存的路径，备注：需要自己创建
    
  },
  filename: function (req, file, cb) {
      // 将保存文件名设置为 时间戳+原名，比如 1478521468943-logo
      var name = Date.now()+'-'+file.originalname;
      cb(null, name);
      
  }
});

// 通过 storage 选项来对 上传行为 进行定制化
 var uploadStorage = multer({ storage: storage })
//多图上传
router.post('/uploadFiles', uploadStorage.array('file', 100), function(req, res, next){
  console.log(req.body.fileclass)
  let files = []
  for(let file of req.files){
    file['fileclass'] = req.body.fileclass
    files.push(file)
  }
  // console.log(req.)
  // console.log(files)
  res.json(files)
});
// 单图上传
router.post('/uploadFile', uploadStorage.single('file'), function(req, res, next){
  console.log(req.file)
  res.json(req.file)
});

router.get('/img/:name', function(req, res, next){
  var form = fs.readFileSync(uploadFolder+req.params.name);
  res.send(form);
});
router.get('/assignment/:name', function(req, res, next){
  console.log(req.params.name)
  var assignment = fs.readFileSync('data/assignment/'+req.params.name);
  res.send(assignment);
});
router.get('/result/:name', function(req, res, next){
  console.log(req.params.name)
  var submit = fs.readFileSync('data/result/'+req.params.name);
  res.send(submit);
});


router.post('/mailXieshou', function(req, res){
  // console.log(req.body)
  if(!fs.existsSync('data/assignment/'+req.body.name+'.zip')){
    zipFiles(req.body.name, req.body.urls, req.body.textName, req.body.textContent, 'assignment')
  }
  var mailOptions = {
    from: "service@funtolearn.co",
    to: req.body.email,
    subject: req.body.name, // Subject line
    html: `<p>Hi ${req.body.userName},</p>
    <br/>
    <p>A new order <b>${req.body.name}</b> has been assigned to you.</p>
    <p>You can login the <a href="https://console.funtolearn.co">Fun to Learn system</a> using your given email&passsword to get more detail.
    And you can do more operations on it.</p>
    <br/>
    <p>Download the compressed assignment materials directly: <a href="https://console.funtolearn.co/api/assignment/${req.body.name}.zip">${req.body.name}</a>. 
    </p><p>The .zip file includes all materials customer required. </p>
    <p>Please contact Fun to Learn using wechat or replying this email if you have any question. </p>
    <p>If the download doesn't triggered, please try using Chrome browser and press ctlr+shift+R to refresh the opened page. </p>
    <br/>
    <p>Best Regard,</p>
    <p><a href="https://funtolearn.co">Fun to Learn Team</a></p>`
  };
  // console.log(mailOptions)
  mail.sendMail(mailOptions, function(err, data){
    console.log('mail error', err, 'data', data)
    res.json(data)
  })
})
router.post('/mailCustomer', function(req, res){
  if(!fs.existsSync('data/result/'+req.body.name+'.zip')){
    zipFiles(req.body.name, req.body.urls, req.body.textName, req.body.textContent, 'result')
  }
  var mailOptions = {
    from: "service@funtolearn.co",
    to: req.body.email,
    subject: req.body.name, // Subject line
    html: `<p>Hi ${req.body.userName},</p>
    <br/>
    <p>The result for order <b>${req.body.name}</b> has been submited to you.</p>
    <br/>
    <p>Please download the compressed result directly by clicking <a href="https://console.funtolearn.co/api/result/${req.body.name}.zip">${req.body.name}</a>. 
    </p><p>The .zip file includes all materials and assistant notification. </p>
    <br/>
    <p>If the download doesn't triggered, please try using Chrome browser and press ctlr+shift+R to refresh the opened page. </p>
    <p>Please contact Fun to Learn using wechat or replying this email if you have any question. </p>
    <br/>
    <p>Best Regard,</p>
    <p><a href="https://funtolearn.co">Fun to Learn Team</a></p>`
  };
  // console.log(mailOptions)
  mail.sendMail(mailOptions, function(err, data){
    console.log('mail error', err, 'data', data)
    res.json(data)
  })
})


module.exports = router;
